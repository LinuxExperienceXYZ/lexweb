+++
title = "About"
layout = "about"
description = "Musings on free and open-source software by Tom Furnari"
date = "2019-02-28"
aliases = ["about-us","about-tom","about-linuxexperience.xyz"]
author = "Tom Furnari"
image = '/img/about/tom_hed.jpg'	
+++

![Tom](/img/about/tom_hed.jpg)

The Linux Experience XYZ is a video channel and Website that started in the summer of 2020 to explore the Linux operating system. Examining the use of free and open-source software (FOSS) and related technologies, The Linux Experience also reports on news and happenings in the community as well as other developments that can impact FOSS and its users.

Hugo makes use of a variety of open source projects including:

* https://github.com/yuin/goldmark
* https://github.com/alecthomas/chroma
* https://github.com/muesli/smartcrop
* https://github.com/spf13/cobra
* https://github.com/spf13/viper

Hugo is ideal for blogs, corporate websites, creative portfolios, online magazines, single page applications or even a website with thousands of pages.

Hugo is for people who want to hand code their own website without worrying about setting up complicated runtimes, dependencies and databases.

Websites built with Hugo are extremelly fast, secure and can be deployed anywhere including, AWS, GitHub Pages, Heroku, Netlify and any other hosting provider.

Learn more and contribute on [GitHub](https://github.com/gohugoio).

