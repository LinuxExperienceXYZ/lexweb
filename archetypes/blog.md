+++
author = ""
title = ""
date = ""
description = ""
tags = [""]
categories = [""]
images = [""]
+++





If you found this video useful, please like and subscribe!

{{< rawhtml >}}
  <p><a href="https://www.youtube.com/channel/UC733VVsJYjGpDqQOebcoQIQ">
<img src="/img/like-512.webp" alt="Like and subscribe!" style="width:45px;height:45px;">
</a></p>
{{< /rawhtml>}}


Patrons enjoy exclusive benefits for regular contributions:
{{< rawhtml >}}
  <p><a href="https://www.patreon.com/bePatron?u=38908364" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script></p>
{{< /rawhtml>}}

Or, a one time donation is also very much appreciated:
{{< rawhtml >}} 
<p><a href="https://paypal.me/tomfur">
<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png" alt="PayPal.me">
</a></p>
{{< /rawhtml >}} 
